clear;
clc;
close all;

mrstModule add ad-core;

%% Creating mesh
path = '.\mesh\';

mesh = UniversalFracturedMesh(path,'mt_terri','porepy');

mesh = mesh.build_master_slave_lists();
mesh = mesh.process_intersections();
figure(1);
plot_mesh(mesh);


%% solver
solver = SinglePhaseFlowSolver(mesh,'fracture_only',true);

%% Setting BCs
dP = 10*psia;
side = 'ymin';
medium = 'f';
solver = solver.set_pressure_bc(dP, side, medium, 'tol', 1.1);
% medium = 'm';
% solver = solver.set_pressure_bc(dP, side, medium);

side = 'ymax';
medium = 'f';
solver = solver.set_pressure_bc(0, side, medium, 'tol', 1.1);  
% medium = 'm';
% solver = solver.set_pressure_bc(0, side, medium);

%% Setting fluid
nf = solver.virtual_cells.num;
nw = ones(nf,1);
nn = ones(nf,1);
nw = interp1([1,21],[1,6],solver.virtual_cells.fracs);
nn = nw;

figure(999)
colormap(jet);
p = plot_fracture_data(solver.mesh,nw);
colorbar;


solver = solver.set_fluid('muw',1,'mun',1,'cw',0,'cn',0,...
                          'rhow',1,'rhon',1,'pref',0,...
                          'nw',nw,...
                          'nn',nn);
                      
%% Updating fracture properties
ap = 1*milli;
kf = ap.^2/12;
phif = 1;
fprops = constant_fracture_properties( solver.mesh, ap,kf,phif);
solver = solver.update_fracture_properties(fprops);

%% Computing transmissibility
solver = solver.compute_transmissibility();

%% Initializing state
state = initialize_state(solver, 1, 0*psia);

fig = figure('Position',[100,100,1200,500]);

%% Solving Pressure
[state] = solver.solve_pressure(state);
[pnm, pnf] = split_data(solver,state.pn);

%% Plotting fracture field
figure(fig)
colormap(jet);
p = plot_fracture_data(solver.mesh,pnf/psia);
colorbar;
axis equal;
set(gca,'FontSize',16);
drawnow;

direction = 'y';
keff = effective_permeability(solver, state.bc_fluxes, direction, dP);
fprintf('\n\nkeff = %f\n\n',keff/(milli*darcy));

%% Initializing vectors
level = 0:0.001:1;
swavg = zeros(length(level),1);
krw_ups = zeros(length(level),1);
krn_ups = zeros(length(level),1);

for i = 1:length(level)
    
    [swinit] = invert_fractional_flow(solver,level(i));

    %% compute average saturation
    swavg(i) = average_saturation(solver,swinit,ap);

    %% Initializing state
    state = initialize_state(solver, swinit, 0*psia);

    %% Solving Pressure
    [state] = solver.solve_pressure(state);

    %% Computing effective mobility
    qw = state.bc_fluxes.val;
    qw = sum(qw(qw > 0));
    G = solver.mesh.G;
    x_size = max(G.nodes.coords(:,1));
    y_size = max(G.nodes.coords(:,1));
    L = y_size;
    A = x_size;
    mobeff = qw*L/(dP*A);

    krw_ups(i) = solver.fluid_info.muw*level(i)*mobeff/keff;
    krn_ups(i) = solver.fluid_info.mun*(1-level(i))*mobeff/keff;
    
end

figure(3)
plot(swavg,krw_ups,'LineStyle','--','LineWidth',2.0);
hold on;
for nw_f = 1:6
    plot(swavg,swavg.^nw_f,'LineWidth',1.5,'LineStyle',':');
    hold on;
end

