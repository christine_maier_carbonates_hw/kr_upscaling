function [ swinit ] = invert_fractional_flow( solver, level )
    
    nc = solver.virtual_cells.num;
    swinit = zeros(nc,1);
    
    sw = 0:0.01:1;
    for i = 1:nc
        nw = solver.fluid_info.nw(i);
        nn = solver.fluid_info.nn(i);
        krw = @(sw)sw.^nw;
        krn = @(sw)(1-sw).^nn;
        mobw = @(sw) krw(sw)/solver.fluid_info.muw;
        mobn = @(sw) krn(sw)/solver.fluid_info.mun;
        mobt = @(sw) mobw(sw)+mobn(sw);
        fw = @(sw)mobw(sw)./mobt(sw);
        
        fwsw = fw(sw);
        swinit(i) = interp1(fwsw,sw,level);
      
    end
    

end

