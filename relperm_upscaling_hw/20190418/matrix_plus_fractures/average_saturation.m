function [ sw_avg ] = average_saturation( solver, swinit, aperture )

    medges = solver.virtual_cells.master_slave_info.medges;
    areas = solver.mesh.G.faces.areas(medges);
    vm = solver.mesh.G.cells.volumes;
    vf = areas*aperture;
    pf = ones(solver.virtual_cells.num,1);
    vols = [vm;vf];
    poro = [solver.matrix_rock.poro; pf];
    sw_avg = sum(swinit.*poro.*vols)/sum(poro.*vols);
    sw_avg(sw_avg < 0) = 0;  sw_avg(sw_avg > 1) = 1;
    
end

