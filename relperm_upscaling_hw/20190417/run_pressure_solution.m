clear;
clc;
close all;

mrstModule add ad-core;

%% Creating mesh
load('ab_2_mesh.mat');
load('swinit.mat');

%% solver
solver = SinglePhaseFlowSolver(mesh,'fracture_only',false);

%% Setting BCs
dP = 10*psia;
side = 'ymin';
medium = 'f';
solver = solver.set_pressure_bc(dP, side, medium, 'tol', 5.1);
% medium = 'm';
% solver = solver.set_pressure_bc(dP, side, medium);

side = 'ymax';
medium = 'f';
solver = solver.set_pressure_bc(0, side, medium, 'tol', 5.1);  
% medium = 'm';
% solver = solver.set_pressure_bc(0, side, medium);

%% Setting fluid
nm = solver.mesh.G.cells.num;
nf = solver.virtual_cells.num;
matrix_cells = 1:nm;
fracture_cells = nm+1:nm+nf;

nw = ones(nm+nf,1);
nn = ones(nm+nf,1);
nw(fracture_cells) = 6;
nn(fracture_cells) = 6;

solver = solver.set_fluid('muw',1,'mun',1,'cw',0,'cn',0,...
                          'rhow',1,'rhon',1,'pref',0,...
                          'nw',nw,...
                          'nn',nn);
                      
%% Setting matrix rock
km = 1*nano*darcy;
phi = 0.05;
matrix_rock = makeRock(solver.mesh.G, km, phi);
solver = solver.set_matrix_rock(matrix_rock);

%% Setting buffer
left_cells = find(solver.mesh.G.cells.centroids(:,1)<5);
bottom_cells = find(solver.mesh.G.cells.centroids(:,2)<5);
right_cells = find(solver.mesh.G.cells.centroids(:,1)>60-5);
top_cells = find(solver.mesh.G.cells.centroids(:,2)>60-5);
matrix_rock.perm(bottom_cells,:) = 10000*darcy;
matrix_rock.perm(top_cells,:) = 10000*darcy;
matrix_rock.perm(left_cells,:) = km/100000;
matrix_rock.perm(right_cells,:) = km/100000;

%% Updating fracture properties
ap = 0.4*milli;
kf = ap.^2/12;
phif = 1;
fprops = constant_fracture_properties( solver.mesh, ap,kf,phif);
solver = solver.update_fracture_properties(fprops);

%% Computing transmissibility
solver = solver.compute_transmissibility();

%% Initializing state
state = initialize_state(solver, swinit, 0*psia);

%% 
swavg = average_saturation(solver,swinit,ap);

fig = figure('Position',[100,100,1200,500]);

%% Solving Pressure
[state] = solver.solve_pressure(state);
[pnm, pnf] = split_data(solver,state.pn);

%% Plotting fracture field
figure(fig)
colormap(jet);
p = plotCellData(solver.mesh.G,pnm/psia);
p.EdgeAlpha = 0.1;
p = plot_fracture_data(solver.mesh,pnf/psia);
colorbar;
axis equal;
set(gca,'FontSize',16);
drawnow;

%% Computing effective mobility
qw = state.bc_fluxes.val;
qw = sum(qw(qw > 0));
G = solver.mesh.G;
x_size = max(G.nodes.coords(:,1));
y_size = max(G.nodes.coords(:,1));
L = y_size;
A = x_size;
mobeff = qw*L/(dP*A);

keff = 64.5111*milli*darcy;

level = 0.25;
krw_ups = 1*level*mobeff/keff;
krn_ups = 1*(1-level)*mobeff/keff;

% direction = 'y';
% keff = effective_permeability(solver, state.bc_fluxes, direction, dp);
% keff/(milli*darcy)

