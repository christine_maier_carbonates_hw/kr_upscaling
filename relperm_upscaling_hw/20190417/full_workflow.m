clear;
clc;
close all;

mrstModule add ad-core;

%% Creating mesh
load('ab_2_mesh.mat');

%% solver
solver = SinglePhaseFlowSolver(mesh,'fracture_only',false);

%% Setting BCs
dP = 10*psia;
side = 'ymin';
medium = 'f';
solver = solver.set_pressure_bc(dP, side, medium, 'tol', 5.1);
medium = 'm';
solver = solver.set_pressure_bc(dP, side, medium);

side = 'ymax';
medium = 'f';
solver = solver.set_pressure_bc(0, side, medium, 'tol', 5.1);  
medium = 'm';
solver = solver.set_pressure_bc(0, side, medium);

%% Setting fluid
nm = solver.mesh.G.cells.num;
nf = solver.virtual_cells.num;
matrix_cells = 1:nm;
fracture_cells = nm+1:nm+nf;

nw_f = 2; nn_f = 2;
nw_m = 2; nn_m = 2;

nw = ones(nm+nf,1);
nn = ones(nm+nf,1);
nw(fracture_cells) = nw_f;
nn(fracture_cells) = nn_f;
nw(matrix_cells) = nw_m;
nn(matrix_cells) = nn_m;

solver = solver.set_fluid('muw',1,'mun',1,'cw',0,'cn',0,...
                          'rhow',1,'rhon',1,'pref',0,...
                          'nw',nw,...
                          'nn',nn);
                      
%% Setting matrix rock
km = 1*milli*darcy;
phi = 0.1;
matrix_rock = makeRock(solver.mesh.G, km, phi);
solver = solver.set_matrix_rock(matrix_rock);

%% Setting buffer
left_cells = find(solver.mesh.G.cells.centroids(:,1)<5);
bottom_cells = find(solver.mesh.G.cells.centroids(:,2)<5);
right_cells = find(solver.mesh.G.cells.centroids(:,1)>60-5);
top_cells = find(solver.mesh.G.cells.centroids(:,2)>60-5);
matrix_rock.perm(bottom_cells,:) = 10000*darcy;
matrix_rock.perm(top_cells,:) = 10000*darcy;
matrix_rock.perm(left_cells,:) = km/100000;
matrix_rock.perm(right_cells,:) = km/100000;

%% Updating fracture properties
ap = 0.4*milli;
% kf = ap.^2/12;
kf = km;
phif = 1;
fprops = constant_fracture_properties( solver.mesh, ap,kf,phif);
solver = solver.update_fracture_properties(fprops);

%% Computing transmissibility
solver = solver.compute_transmissibility();

%% Initializing state
state = initialize_state(solver, 1, 0*psia);

fig = figure('Position',[100,100,1200,500]);

%% Solving Pressure
[state] = solver.solve_pressure(state);
[pnm, pnf] = split_data(solver,state.pn);

%% Plotting fracture field
figure(fig)
colormap(jet);
p = plotCellData(solver.mesh.G,pnm/psia);
p.EdgeAlpha = 0.1;
p = plot_fracture_data(solver.mesh,pnf/psia);
colorbar;
axis equal;
set(gca,'FontSize',16);
drawnow;

direction = 'y';
keff = effective_permeability(solver, state.bc_fluxes, direction, dP);
keff/(milli*darcy)

%% upscaling relperm
level = [0:0.1:1];
swavg = zeros(length(level),1);
krw_ups = zeros(length(level),1);
krn_ups = zeros(length(level),1);
for i=1:length(level)
    [swinit] = invert_fractional_flow(solver,level(i));
    [swinit_m, swinit_f] = split_data(solver,swinit);
    
    %% compute average saturation
    swavg(i) = average_saturation(solver,swinit,ap);
    
    %% Initializing state
    state = initialize_state(solver, swinit, 0*psia);
    
    %% Solving Pressure
    [state] = solver.solve_pressure(state);

    %% Computing effective mobility
    qw = state.bc_fluxes.val;
    qw = sum(qw(qw > 0));
    G = solver.mesh.G;
    x_size = max(G.nodes.coords(:,1));
    y_size = max(G.nodes.coords(:,1));
    L = y_size;
    A = x_size;
    mobeff = qw*L/(dP*A);

    krw_ups(i) = solver.fluid_info.muw*level(i)*mobeff/keff;
    krn_ups(i) = solver.fluid_info.mun*(1-level(i))*mobeff/keff;
end
%% 

sw = (0:0.01:1);
figure()
hold;
plot(sw,sw.^nw_m,'DisplayName','krw_m','LineStyle','--','LineWidth',1)
plot(sw,(1-sw).^nn_m,'DisplayName','krn_m','LineStyle','--','LineWidth',1)
plot(sw,sw.^nw_f,'DisplayName','krw_f','LineStyle','--','LineWidth',1)
plot(sw,(1-sw).^nn_f,'DisplayName','krn_f','LineStyle','--','LineWidth',1)
plot(swavg,krw_ups,'DisplayName','krwups','LineWidth',2)
plot(swavg,krn_ups,'DisplayName','krnups','LineWidth',2)
axis equal;
set(gca,'FontSize',16);
drawnow;


