clear;
clc;
close all;

mrstModule add ad-core;

%% Creating mesh
load('ab_2_mesh.mat');

%% solver
solver = SinglePhaseFlowSolver(mesh,'fracture_only',false);

%% Setting BCs
dp = 10*psia;
value = dp;
side = 'ymin';
medium = 'f';
solver = solver.set_pressure_bc(value, side, medium, 'tol', 5.1);

value = 0*psia;
side = 'ymax';
medium = 'f';
solver = solver.set_pressure_bc(value, side, medium, 'tol', 5.1);  

%% Setting fluid
nm = solver.mesh.G.cells.num;
nf = solver.virtual_cells.num;
matrix_cells = 1:nm;
fracture_cells = nm+1:nm+nf;

nw = ones(nm+nf,1);
nn = ones(nm+nf,1);
nw(fracture_cells) = 6;
nn(fracture_cells) = 6;

solver = solver.set_fluid('muw',1,'mun',1,'cw',0,'cn',0,...
                          'rhow',1,'rhon',1,'pref',0,...
                          'nw',nw,...
                          'nn',nn);
                      
%% Setting matrix rock
km = 1*nano*darcy;
phi = 0.05;
matrix_rock = makeRock(solver.mesh.G, km, phi);
solver = solver.set_matrix_rock(matrix_rock);

%% Setting buffer
left_cells = find(solver.mesh.G.cells.centroids(:,1)<5);
bottom_cells = find(solver.mesh.G.cells.centroids(:,2)<5);
right_cells = find(solver.mesh.G.cells.centroids(:,1)>60-5);
top_cells = find(solver.mesh.G.cells.centroids(:,2)>60-5);
matrix_rock.perm(bottom_cells,:) = 10000*darcy;
matrix_rock.perm(top_cells,:) = 10000*darcy;
matrix_rock.perm(left_cells,:) = km/100000;
matrix_rock.perm(right_cells,:) = km/100000;

%% Updating fracture properties
ap = 0.4*milli;
kf = ap.^2/12;
phif = 1;
fprops = constant_fracture_properties( solver.mesh, ap,kf,phif);
solver = solver.update_fracture_properties(fprops);

%% Computing transmissibility
solver = solver.compute_transmissibility();

%% Initializing state
state = initialize_state(solver, 1, 0*psia);

fig = figure('Position',[100,100,1200,500]);

%% Solving Pressure
[state] = solver.solve_pressure(state);
[pnm, pnf] = split_data(solver,state.pn);

%% Plotting fracture field
figure(fig)
colormap(jet);
p = plotCellData(solver.mesh.G,pnm/psia);
p.EdgeAlpha = 0.1;
p = plot_fracture_data(solver.mesh,pnf/psia);
colorbar;
axis equal;
set(gca,'FontSize',16);
drawnow;

direction = 'y';
keff = effective_permeability(solver, state.bc_fluxes, direction, dp);
keff/(milli*darcy)

%% upscaling relperm
level = 0.25;
[swinit] = invert_fractional_flow(solver,level);
[swinit_m, swinit_f] = split_data(solver,swinit);

figure(fig)
colormap(jet);
p = plotCellData(solver.mesh.G,swinit_m);
p.EdgeAlpha = 0.1;
p = plot_fracture_data(solver.mesh,swinit_f);
colorbar;
axis equal;
set(gca,'FontSize',16);
drawnow;
