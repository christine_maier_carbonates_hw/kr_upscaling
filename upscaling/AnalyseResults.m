clear all; 
close all;

load('RossenKumarRelPerm.mat');

%% plot fracture relperm

sw = 0:0.01:1;
figure()
hold()
for i=1:3
    plot(sw,HD(i).kwm(1)*sw.^HD(i).n(1),'DisplayName',['krw ',num2str(i)],'LineStyle','-.','LineWidth',2);
    plot(sw,HD(i).kwm(2)*(1-sw).^HD(i).n(2),'DisplayName',['krn ',num2str(i)],'LineStyle','-.','LineWidth',2);
end
plot(sw,0.3*sw.^2,'DisplayName',['krw matrix'],'LineStyle','-.','LineWidth',2);
plot(sw,0.8*(1-sw).^2,'DisplayName',['krn matrix'],'LineStyle','-.','LineWidth',2);

figure();
hold();
for i=1:3
    load(['frac_bc_8_8_8_3D_HD' int2str(i) '_prod_data.mat']);
    plot(pv_inj, oil_rate(1,:)./stb.*day)
    plot(pv_inj, water_rate(1,:)./stb.*day)
    plot(pv_inj, oil_rate_ups(1,:)./stb.*day)
    plot(pv_inj, water_rate_ups(1,:)./stb.*day)
end

ref = load(['frac_bc_8_8_8_3D_HD1_prod_data.mat']);

figure();
hold();
for i=1:3
    load(['frac_bc_8_8_8_3D_HD' int2str(i) '_prod_data.mat'])
    plot(pv_inj, (oil_rate(1,:)-ref.oil_rate(1,:))./stb.*day)
    plot(pv_inj, (water_rate(1,:)-ref.water_rate(1,:))./stb.*day)
end

figure();
hold();
for i=1:3
    load(['frac_bc_8_8_8_3D_HD' int2str(i) '_prod_data.mat']);
    plot(pv_inj, (oil_rate_ups(1,:)-ref.oil_rate(1,:))./stb.*day)
    plot(pv_inj, (water_rate_ups(1,:)-ref.water_rate(1,:))./stb.*day)
end

figure();
hold();
for i=1:3
    load(['frac_bc_8_8_8_3D_HD' int2str(i) '.mat']);
    plot(sw, krw(:,1))
    plot(sw, krn(:,1))
end
plot(sw,0.3*sw.^2,'DisplayName',['krw matrix'],'LineStyle','-.','LineWidth',2);
plot(sw,0.8*(1-sw).^2,'DisplayName',['krn matrix'],'LineStyle','-.','LineWidth',2);



state = initState(G, [], 0,[0,1]);
[mu, ~, ~, ~, ~, ~] = fluid.properties(state);

figure()
hold;
for i=1:7
    load(['frac_bc_8_8_8_3D_HD' int2str(i) '.mat'])
    s = sw';
    krwsw=@(sw)interp1(s,krw(:,1)',sw);
    krnsw=@(sw)interp1(s,krn(:,1)',sw);
    mobwsw = @(sw) krwsw(sw)/mu(1);
    mobnsw = @(sw) krnsw(sw)/mu(2);
    fsw = @(sw) mobwsw(sw)./(mobwsw(sw)+mobnsw(sw));
    plot(s,fsw(s))

end
