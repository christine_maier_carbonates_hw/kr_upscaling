function [pc] = pc_corey_lin_buffer(sw,Pe,np,snt,srw,srn)
   
   pc = pc_corey(sw,Pe,np,srw,srn);
   coreyind = find(sw>(1-snt-srn));
   % pc(coreyind) = (Pe/snt)*(((1-snt-srw)/(1-srw)).^(-np)).*(1-sw(coreyind));
   if ~isempty(coreyind)
       pc(coreyind) = max(0,...
           (Pe/snt)*(((1-snt-srw-srn)/(1-srw-srn))^(-np)).*(1-sw(coreyind)-srn));
   end
end