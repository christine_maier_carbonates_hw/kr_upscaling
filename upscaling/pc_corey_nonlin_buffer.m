function [pc] = pc_corey_nonlin_buffer(sw,Pe,np,snt,srw,srn)
   
   pc = pc_corey(sw,Pe,np,srw,srn);
   coreyind = find((sw-srw)/(1-srw-srn)>(1-snt));
   % pc(coreyind) = (Pe/snt)*(((1-snt-srw)/(1-srw)).^(-np)).*(1-sw(coreyind));
   if ~isempty(coreyind)
       pc(coreyind) = max(0,...
           (Pe/(1-snt))*(((sw(coreyind)-(1-snt)-srw)/(1-srw-srn)).^(-np)).*(1-(sw(coreyind)-srw)/(1-srw-srn)));
   end
end
