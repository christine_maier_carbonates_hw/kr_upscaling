function [ k ] = upscale_DFN_perm( G , rock, T)
%UPSCALE_PERM Summary of this function goes here
%   Detailed explanation goes here

%  Compute perm in x-direction:
%
%                  v*n = 0
%              ______________
% l_boundary  |              | r_boundary
%             |              |
%     p1 = 1  |              | p2 = 0
%             |______________|
%                   v*n = 0
%
%             |--------------|
%                    L
%
%           grad p = (p2 - p1)/L
%           q1: flux over right boundary
%           kx* = (q1*L)/(area(r_boundary)*(p2 - p1))

   fluid    = initSingleFluid('mu' , 1 * Pascal*second, ...
                              'rho', 1 * kilogram/meter^3);
   dims     = find(G.cartDims);

   k = zeros(1, numel(dims));
   rSol = initResSol(G, 0.0);

   frac_bside = {{'West', 'East'}, {'South', 'North'}, {'Top', 'Bottom'}};    
   frac_bfaces=findfracboundaryfaces(G,1.0e-5);   

   use_trans=true;
   if isempty(T)
       T = computeTrans(G, rock);
       use_trans=false;
   end

   for i = dims
      bfaces{1,i} = frac_bfaces.(frac_bside{i}{1})';
      bc = addBC([],frac_bfaces.(frac_bside{i}{1}),'pressure',1*barsa);

      bfaces{2,i} = frac_bfaces.(frac_bside{i}{2})';
      bc = addBC(bc,frac_bfaces.(frac_bside{i}{2}),'pressure',0*barsa);

      rSol = incompTPFA(rSol, G, T, fluid, 'bc', bc, ...
                       'LinSolve', @mldivide, 'use_trans',use_trans);

      area = sum(G.faces.areas(bfaces{2,i}));
      L    = abs(G.faces.centroids(bfaces{1,i}(1), i) - ...
                 G.faces.centroids(bfaces{2,i}(1), i));

      q    = abs(sum(rSol.flux(bfaces{2,i})));
      k(i) = q * L / (1*barsa*area);
   end
end

