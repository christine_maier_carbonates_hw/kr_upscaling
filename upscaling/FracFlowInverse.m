  function [ sw ] = FracFlowInverse( mu, n, kwm, reg_tag, Fw_level)  
   % definition of fractional flow function
   krwsw = @(sw,reg_tag) kwm{reg_tag}(1)*sw.^n{reg_tag}(1);
   krnsw = @(sw,reg_tag) kwm{reg_tag}(2)*(1-sw).^n{reg_tag}(2);
   mobwsw = @(sw,reg_tag) krwsw(sw,reg_tag)/mu(1);
   mobnsw = @(sw,reg_tag) krnsw(sw,reg_tag)/mu(2);
   fsw = @(sw,reg_tag) mobwsw(sw,reg_tag)./(mobwsw(sw,reg_tag)+mobnsw(sw,reg_tag));

   s = 0:0.01:1;
   fval  = fsw(s,reg_tag);
   inv_fsw=@(f)interp1(fval,s,f);
   
   sw = inv_fsw(Fw_level);
   
  end