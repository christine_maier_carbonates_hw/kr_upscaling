function [] = Create_DWong_fracs_only(a_tol_min, a_tol_max)
%% load grid and fracture data

data = load('Set_02.mat');
celldim = data.celldim;
physdim = data.physdim;
G = cartGrid(celldim, physdim); 
G = computeGeometry(G);
tol = data.tol;

G.rock=makeRock(G,data.G_global.Matrix.rock.perm,data.G_global.Matrix.rock.poro);

%% Set up fracture planes
% Fracture planes are set up by defining their vertices. Additionally,
% the aperture, porosity and permeability of the fractures are provided.

ix = ([data.fracplanes.area]<=a_tol_max & [data.fracplanes.area]>a_tol_min);
fracplanes = data.fracplanes(ix);

for i=1:size(fracplanes,2)
fracplanes(i).SetID= [];
fracplanes(i).twinsetID= [];
fracplanes(i).processed= false;
fracplanes(i).intersects= [];
fracplanes(i).fracgrid= [];
end

[G,fracplanes]=EDFMgrid(G,fracplanes,...
    'Tolerance',1.0e-9,'fracturelist',1:size(fracplanes,2));


figure; plotGrid(G,'facealpha',0);
for i = 1:numel(fieldnames(G.FracGrid))
    plotGrid(G.FracGrid.(['Frac',num2str(i)]));
end
view(15,20);
%% Fracture-Matrix non-Neighbouring Connections (NNC)
% This calculates the transmissibilities between connected matrix and
% fracture grid blocks. Information is saved under G.nnc.

% G=fracturematrixNNC3D(G,tol);

%% Fracture-Fracture NNCs
% This calculates the transmissibilities between connected fracture and
% fracture grid blocks. Information is saved under G.nnc.

[G,fracplanes]=fracturefractureNNCs3D(G,fracplanes,tol,'Verbose',true);
%% Setup TPFA Operators
% Generate operators for the black oil model which incorporates all the
% NNCs identified above.

TPFAoperators = setupEDFMOperatorsTPFA(G, G.rock, tol);

%% Remove frac-mat NNCs (MODIFICATION: to decouple matrix from fracture network)
fracmatnnc_index = cellfun(@(x) strcmp(x,'frac-matrix'),G.nnc.type);
G.nnc.cells=G.nnc.cells(~fracmatnnc_index,:);
G.nnc.T=G.nnc.T(~fracmatnnc_index);
G.nnc.type=G.nnc.type(~fracmatnnc_index);
G.nnc.CI=[];
%% Assemble global grid and compute transmissibilities
% In this section, we combine the fracture and matrix grids into one grid.
% The transmissibility for each face in the combined grid and each NNC is
% computed and stored in the vector T.
T = computeTrans(G, G.rock);
cf = G.cells.faces(:,1);
nf = G.faces.num;
T  = 1 ./ accumarray(cf, 1./T, [nf, 1]);
T = [T;G.nnc.T];


save(['Create_DWong_' int2str(a_tol_min) '_' int2str(a_tol_max) '.mat'],'G','fracplanes','TPFAoperators','T')

end

