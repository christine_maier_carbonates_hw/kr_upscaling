%% Water injection into a 3D fractured porous media
% Two-phase example with vertical producer/injector pair simulating
% water injection in a 3-dimensional fractured porous media using the EDFM
% method
clear all;
close all;
%% Load necessary modules
mrstModule add hfm;             % hybrid fracture module
mrstModule add mrst-gui;        % plotting routines
mrstModule add ad-blackoil;     % AD blackoil solver
mrstModule add ad-core;         % AD core module
mrstModule add ad-props;        % AD properties
mrstModule add agmg;
mrstModule add incomp

%% Set up a matrix grid
% We first set up a Cartesian matrix grid with dimensions 500m x 200m x
% 20m with grid blocks that are 10m x 10m x 10m. Matrix permeability is
% 100mD and porosity is 30%.

celldim = [10, 10, 10];
physdim = [20, 20, 20];
G = cartGrid(celldim, physdim); 
G = computeGeometry(G);

K = 0.1 *milli*darcy;
porosity = 0.1
G.rock=makeRock(G,K,porosity);

K_frac = 100*darcy;
porosity_frac = 0.99;


%% Set up fracture planes
% Fracture planes are set up by defining their vertices. Additionally,
% the aperture, porosity and permeability of the fractures are provided.
% Fracture planes 1 and 3 will be vertical while fracture 2 is slanted.

fracplanes = Create3DFractureSet(G,8,8,8, 0.0, 'intersection_orth');

for i=1:size(fracplanes,2)
fracplanes(i).aperture = 0.001;
fracplanes(i).poro=porosity_frac;
fracplanes(i).perm=K_frac;    
end

%% Construct fracture grid
% The fracture grid is constructed using the matrix grid. The matrix grid
% will serve as a 'cookie cutter' to subdivide the fracture planes.
% Parallel processing can be used to speed up this process (Start a 
% parallel pool to do this).

tol=1e-5;
[G,fracplanes]=EDFMgrid(G,fracplanes,...
    'Tolerance',tol,'fracturelist',1:size(fracplanes,2));


figure; plotGrid(G,'facealpha',0);
for i = 1:numel(fieldnames(G.FracGrid))
    plotGrid(G.FracGrid.(['Frac',num2str(i)]));
end
view(15,20);
%% Fracture-Matrix non-Neighbouring Connections (NNC)
% This calculates the transmissibilities between connected matrix and
% fracture grid blocks. Information is saved under G.nnc.

tol=1e-5;
G=fracturematrixNNC3D(G,tol);

%% Fracture-Fracture NNCs
% This calculates the transmissibilities between connected fracture and
% fracture grid blocks. Information is saved under G.nnc.

tol=1e-5;
[G,fracplanes]=fracturefractureNNCs3D(G,fracplanes,tol,'Verbose',true);

%% Setup TPFA Operators
% Generate operators for the black oil model which incorporates all the
% NNCs identified above.

TPFAoperators = setupEDFMOperatorsTPFA(G, G.rock, tol);
%% Assemble global grid and compute transmissibilities
% In this section, we combine the fracture and matrix grids into one grid.
% The transmissibility for each face in the combined grid and each NNC is
% computed and stored in the vector T.
T = computeTrans(G, G.rock);
cf = G.cells.faces(:,1);
nf = G.faces.num;
T  = 1 ./ accumarray(cf, 1./T, [nf, 1]);
T = [T;G.nnc.T];

%% Compute upscaled permeability
Keff = upscale_perm(G,G.rock,T);

%% Fluid
muw = 1*centi*poise;
mun = 2*centi*poise;

%Here you define the region id for each cell                       
regiontags = zeros(G.cells.num,1);    
regiontags(1:G.Matrix.cells.num) = 1;
F = fieldnames(G.FracGrid);
for i = 1:numel(F)
%     regiontags(F(i).cells.start:F(i).cells.start+F(i).cells.num-1) = i+1;
    regiontags(G.FracGrid.(F{i}).cells.start:G.FracGrid.(F{i}).cells.start+G.FracGrid.(F{i}).cells.num-1) = 2;
end
figure();
plotCellData(G,regiontags);
xlabel('x')
ylabel('y')
zlabel('z')

frp_data = load('RossenKumarRelPerm.mat');
data_set = 1;

%% fluid
% Here you define corey parameters, one element for each region
n = {[2,2],...
     [frp_data.HD(data_set).n(1),frp_data.HD(data_set).n(2)]...
%     ,[1.4,1.4],...
%     [1.6,1.6]...
     };
sr = {[0.0,0.0],...
      [0.0,0.0]...
%      ,[0,0],...
%      [0,0]
      };
kwm = {[0.3,0.8],...
       [frp_data.HD(data_set).kwm(1),frp_data.HD(data_set).kwm(2)]...
%       ,[1,1],...
%       [1,1]
       };
np = {-1,-1};
pc_scale = {0,0};
fluid = initHeterogeneousCoreyFluid('mu' , [  muw, mun],...
                           'rho', [1000,1000]*kilogram/meter^3,...
                           'n'  , n, ...
                           'sr' , sr, ...
                           'kwm', kwm, ... 
                           'regiontags', regiontags);     
upscaled_ADIfluid = initADIFluidHeterogeneous('mu' , [  muw, mun],...
                           'rho', [1000,1000]*kilogram/meter^3,...
                           'n'  , n, ...
                           'sr' , sr, ...
                           'krmax', kwm, ... 
                           'np', np, ... 
                           'pc_scale', pc_scale, ... 
                           'regiontags', regiontags); 

%% Define two phase oil water model
% We define a two phase oil water model and then
% manually pass in the internal transmissibilities and the topological
% neighborship from the embedded fracture grid.

gravity reset off
model = TwoPhaseOilWaterModel(G, G.rock, fluid);
model.operators = TPFAoperators;


%% upscale perm for different fractional flow levels
Fw = [0:0.01:1];

tic
sw = zeros(size(Fw'));
krw = zeros([size(Fw,2) size(G.cartDims,2)]);
krn = zeros([size(Fw,2) size(G.cartDims,2)]);
for i=1:size(Fw,2)
    [sw(i), krw(i,:), krn(i,:)] = upscale_relperm(G, G.rock, T, fluid, Keff, Fw(i));
end
toc
%% plot relperm 

dims = find(G.cartDims);
dir = ['x','y','z'];

figure1 = figure();
axes1 = axes('Parent',figure1);
hold on;
for i=1:max(regiontags(:))
    plot(sw,kwm{i}(1)*sw.^n{i}(1),'DisplayName',['krw zone ',num2str(i)],'LineStyle','-.','LineWidth',2);
    plot(sw,kwm{i}(2)*(1-sw).^n{i}(2),'DisplayName',['krn zone ',num2str(i)],'LineStyle','-.','LineWidth',2);
end

for i=dims
    plot(sw, krw(:,i),'DisplayName',['upscaled krw_',dir(i)],'LineStyle','-','LineWidth',2);
    plot(sw, krn(:,i),'DisplayName',['upscaled krn_',dir(i)],'LineStyle','-','LineWidth',2);
end
xlabel('Saturation Water [-]','FontWeight','bold');
ylabel('Relative Permeability [-]','FontWeight','bold');
legend1 = legend('show');
set(legend1,'Location','best');
set(axes1,'FontSize',20,'FontWeight','bold','XGrid','on','YGrid','on');

save(['frac_bc_8_8_8_3D_HD' int2str(data_set) '.mat'],'G','fluid', 'upscaled_ADIfluid','T','Keff', 'sw', 'krw', 'krn');

[err_ups, max_err_ups,simtime_ratio] = UpscaleError( G, G.rock, T, upscaled_ADIfluid,...
    Keff, sw, krw, krn);

save(['frac_bc_8_8_8_3D_HD' int2str(data_set) '_err.mat'],'err_ups', 'max_err_ups','simtime_ratio');