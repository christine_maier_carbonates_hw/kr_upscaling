function [G,fracplanes,TPFAoperators,T] = Create3DEDFMData(fsets,frac_tol)
%% Set up a matrix grid
% We first set up a Cartesian matrix grid with dimensions 500m x 200m x
% 20m with grid blocks that are 10m x 10m x 10m. Matrix permeability is
% 100mD and porosity is 30%.

celldim = [10, 10, 10];
physdim = [20, 20, 20];
G = cartGrid(celldim, physdim); 
G = computeGeometry(G);

K = 0.1 *milli*darcy;
porosity = 0.1
G.rock=makeRock(G,K,porosity);

K_frac = 100*darcy;
porosity_frac = 0.99;

%% Set up fracture planes
% Fracture planes are set up by defining their vertices. Additionally,
% the aperture, porosity and permeability of the fractures are provided.
% Fracture planes 1 and 3 will be vertical while fracture 2 is slanted.

fracplanes = Create3DFractureSet(G,fsets,fsets,fsets, frac_tol, 'intersection_orth');

for i=1:size(fracplanes,2)
fracplanes(i).aperture = 0.001;
fracplanes(i).poro=porosity_frac;
fracplanes(i).perm=K_frac;    
end

%% Construct fracture grid
% The fracture grid is constructed using the matrix grid. The matrix grid
% will serve as a 'cookie cutter' to subdivide the fracture planes.
% Parallel processing can be used to speed up this process (Start a 
% parallel pool to do this).

tol=1e-5;
[G,fracplanes]=EDFMgrid(G,fracplanes,...
    'Tolerance',tol,'fracturelist',1:size(fracplanes,2));


figure; plotGrid(G,'facealpha',0);
for i = 1:numel(fieldnames(G.FracGrid))
    plotGrid(G.FracGrid.(['Frac',num2str(i)]));
end
view(15,20);
%% Fracture-Matrix non-Neighbouring Connections (NNC)
% This calculates the transmissibilities between connected matrix and
% fracture grid blocks. Information is saved under G.nnc.

tol=1e-5;
G=fracturematrixNNC3D(G,tol);

%% Fracture-Fracture NNCs
% This calculates the transmissibilities between connected fracture and
% fracture grid blocks. Information is saved under G.nnc.

tol=1e-5;
[G,fracplanes]=fracturefractureNNCs3D(G,fracplanes,tol,'Verbose',true);

%% Setup TPFA Operators
% Generate operators for the black oil model which incorporates all the
% NNCs identified above.

TPFAoperators = setupEDFMOperatorsTPFA(G, G.rock, tol);
%% Assemble global grid and compute transmissibilities
% In this section, we combine the fracture and matrix grids into one grid.
% The transmissibility for each face in the combined grid and each NNC is
% computed and stored in the vector T.
T = computeTrans(G, G.rock);
cf = G.cells.faces(:,1);
nf = G.faces.num;
T  = 1 ./ accumarray(cf, 1./T, [nf, 1]);
T = [T;G.nnc.T];

save(['frac_bc_' int2str(fsets) '_' int2str(fsets) '_' int2str(fsets) '_3D.mat'],'G','fracplanes','TPFAoperators','T')
end