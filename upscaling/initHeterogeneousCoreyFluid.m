function fluid = initHeterogeneousCoreyFluid(varargin)

   opt = struct('mu', [], 'rho', [], 'n', [], 'sr', [], 'kwm', [], 'regiontags', []);
   opt = merge_options(opt, varargin{:});

   prop = @(  varargin) properties(opt, varargin{:});
   kr   = @(s,varargin) relperm(s, opt, varargin{:});

   fluid = struct('properties', prop             , ...
                  'saturation', @(x,varargin) x.s, ...
                  'relperm'   , kr);
end

%--------------------------------------------------------------------------
% Helpers follow.
%--------------------------------------------------------------------------

function varargout = properties(opt, varargin)
   varargout{1}                 = opt.mu ;
   if nargout > 1, varargout{2} = opt.rho; end
   if nargout > 2, varargout{3} = opt.n;   end
   if nargout > 3, varargout{4} = opt.sr;  end
   if nargout > 4, varargout{5} = opt.kwm;  end
   if nargout > 5, varargout{6} = opt.regiontags;  end
%    if nargout > 6, varargout{5} = [];  end
end

%--------------------------------------------------------------------------

function varargout = relperm(s, opt, varargin)
   
    kr = zeros(length(s),2);
    dkr = zeros(length(s),2);
    for i = 1:length(opt.n)
        sr1 = opt.sr{i}(1);
        sr2 = opt.sr{i}(2);
        n1 = opt.n{i}(1);
        n2 = opt.n{i}(2);
        kwm1 = opt.kwm{i}(1);
        kwm2 = opt.kwm{i}(2);
        [s1, s2, ~] = modified_saturations(s, sr1, sr2);
        
        kr(opt.regiontags==i,1:2) = [kwm1 * s1(opt.regiontags==i) .^ n1,...
                                             kwm2 * s2(opt.regiontags==i) .^ n2];
        dkr(opt.regiontags==i,1:2) = [n1* kwm1 * s1(opt.regiontags==i) .^ (n1-1),...
                                      n2* kwm2 * s2(opt.regiontags==i) .^ (n2-1)];
        
    end
    
    varargout{1} = kr;
    varargout{2} = dkr;
    

%    if nargout > 1,
%       null = zeros(size(s1));
%       varargout{2} = [ kwm(1) * n(1) .* s1 .^ (n(1) - 1), ...
%                        null, null                       , ...
%                        kwm(2) * n(2) .* s2 .^ (n(2) - 1)] ./ den;
%    end
% 
%    if nargout > 2,
%       a = n .* (n - 1);
%       varargout{3} = [ kwm(1) * a(1) .* s1 .^ (n(1) - 2), ...
%                        kwm(2) * a(2) .* s2 .^ (n(2) - 2)] ./ den;
%    end
end

%--------------------------------------------------------------------------

function [s1, s2, den] = modified_saturations(s, sr1, sr2)
   den = 1 - sum([sr1,sr2]);
   s1  = (    s(:,1) - sr1) ./ den;  s1(s1 < 0) = 0;  s1(s1 > 1) = 1;
   s2  = (1 - s(:,1) - sr2) ./ den;  s2(s2 < 0) = 0;  s2(s2 > 1) = 1;
end
