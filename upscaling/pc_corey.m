function [pc] = pc_corey(sw,Pe,np,srw,srn)
   
   [swe, ~, ~] = modified_saturations(sw,srw,srn);
   

   pc = Pe*swe.^(-np);
   
   pc(isinf(double(pc)))=1e+10*kilo*Pascal;
   
   
end

function [swe, sne, den] = modified_saturations(sw, srw, srn)
   den = 1 - (srw+srn);
   swe  = (    sw - srw) ./ den;  swe(swe < 0) = 0;  swe(swe > 1) = 1;
   sne  = (1 - sw - srn) ./ den;  sne(sne < 0) = 0;  sne(sne > 1) = 1;
end