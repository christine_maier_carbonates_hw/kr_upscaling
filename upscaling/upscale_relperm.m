function [ sw_avg, krw_ups, krn_ups ] = upscale_relperm( G, rock, Trans, fluid, K_eff, Fw_level)
%UPSCALE_RELPERM Summary of this function goes here
%   Detailed explanation goes here

   dims     = find(G.cartDims);

   krw_ups = zeros(1, numel(dims));
   krn_ups = zeros(1, numel(dims));

   p1 = 10*Pascal;
   p2 = 0*Pascal;
   
   state = initState(G, [], 0,[0,1]);
   
       
   use_trans=true;
   if isempty(Trans)
       Trans = computeTrans(G, rock);
       use_trans=false;
   end

   bside = {{'XMin', 'XMax'}, {'YMin', 'YMax'}, {'ZMin', 'ZMax'}};    
   frac_bside = {{'West', 'East'}, {'South', 'North'}, {'Top', 'Bottom'}};    
   bfaces = cell(2,G.griddim);  
   frac_bfaces=findfracboundaryfaces(G,1.0e-5);   

   %% definition of fractional flow function
   [mu, ~, n, ~, kwm, regiontags] = fluid.properties(state);
   for j=1:G.cells.num
       state.s(j,1) = FracFlowInverse(mu,n,kwm,regiontags(j),Fw_level);
       state.s(j,2) = 1-state.s(j,1);
   end
   % average saturation of the model.
   sw_avg = real(sum(state.s(:,1).*rock.poro(:).*G.cells.volumes(:))/sum(rock.poro(:).*G.cells.volumes(:)));
   sw_avg(sw_avg < 0) = 0;  sw_avg(sw_avg > 1) = 1;

    
   %% upscale relative permeability in each dimensional direction 
   for i = dims
      bc_tmp = pside([],G,bside{i}{1}, p1);      
      bfaces{1,i} = [bc_tmp.face; frac_bfaces.(frac_bside{i}{1})'];
      bc = addBC([],bc_tmp.face,bc_tmp.type,bc_tmp.value,'sat',bc_tmp.sat);
      bc = addBC(bc,frac_bfaces.(frac_bside{i}{1}),'pressure',p1);

      bc_tmp = pside([],G,bside{i}{2}, p2);      
      bfaces{2,i} = [bc_tmp.face; frac_bfaces.(frac_bside{i}{2})'];
      bc = addBC(bc,bc_tmp.face,bc_tmp.type,bc_tmp.value,'sat',bc_tmp.sat);
      bc = addBC(bc,frac_bfaces.(frac_bside{i}{2}),'pressure',p2);
      
      % solve steady state pressure equation
      state_adv = incompTPFA(state, G, Trans, fluid, 'bc', bc, 'use_trans', use_trans);
      %compute effective wetting total mobility
      area = sum(G.faces.areas(bfaces{2,i}));
      L    = abs(G.faces.centroids(bfaces{1,i}(1), i) - ...
                 G.faces.centroids(bfaces{2,i}(1), i));

      q    = sum(state_adv.flux(bfaces{2,i}));
      mobt_eff = q*L/(p1-p2)/area;

      %% compute upscaled krw and krn
      krw_ups(i) = mu(1)*Fw_level*mobt_eff/K_eff(i);
      krn_ups(i) = mu(2)*(1-Fw_level)*mobt_eff/K_eff(i);
      
   end
end

