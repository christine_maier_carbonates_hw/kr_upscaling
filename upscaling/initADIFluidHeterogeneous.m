function fluid = initADIFluidHeterogeneous(varargin)
% SYNOPSIS:
%   fluid = initSimpleADIFluidHeterogeneous()
%   fluid = initSimpleADIFluidHeterogeneous('pn1', 'pv1')
%
% PARAMETERS:
%   'pn'/pv - List of property names/property values.  Possibilities are:
%   - mu  : vector of viscosity values for water, oil and gas, [muW, muO, muG].  
%           Default is [1 1 1].
%   - rho : vector of density values for water, oil and gas, [rhoW, rhoO, rhoG].
%           Default is [1 1 1].
%   - n   : vector of the degrees of the monomials describing relative
%           permeability for water, oil and gas [nW, nO, nG].  Default is
%           [1 1 1] (linear relative permeabilities)
%
% RETURNS:
%   fluid - struct containing the following functions (where X = 'W' [water],
%           'O' [oil] and 'G' [gas]) 
%           * [krW, krO, krG] = relPerm(s_water, s_gas) - relative permeability functions         
%           * rhoX         - density of X
%           * rhoXS        - density of X at surface (equal to rhoX, since incompressible)
%           * bX(p), BX(p) - formation volume factors and their inverses
%                            (constants, always equal one)
%           * muX(p)       - viscosity functions (constant)
%           * krX(s)       - rel.perm for X
%           * krOX(s)      - 
%           * rsSat()      - saturation value for dissolved gas (always returns 0)

   opt = struct('mu', [1 1], 'rho', [1 1], 'sr',[],...
                'n', [],'krmax',[], 'np', [],...
                'pc_scale', [],...
                'c',       [], ...
                'pRef',    0, ...
                'cR',      [],...
                'sbuf',0.1, 'pc_type',1,...
                'regiontags', []);
            
   opt = merge_options(opt, varargin{:});
   
   krW = @(sw, varargin) krw_corey(sw,opt,varargin{:});
   krO = @(so, varargin) krn_corey(so,opt,varargin{:});
   
   relperms = {krW, krO};

   fluid.relPerm = @(sw, sg, varargin) relPerm(krW, krO, sw, varargin{:});

   % adjust for incompTPFA
   prop = @(  varargin) properties(opt, varargin{:});
   fluid.properties = prop;
   fluid.saturation = @(x,varargin) x.s;
   fluid.relperm = @(sw, sg, varargin) relPerm(krW, krO, sw, varargin{:});
   %---------------------------
   
   if(opt.pc_type==1)
       fluid.pcOW = @(sw) pc_corey_heterogeneous(sw,opt);
   elseif(opt.pc_type==2)
       fluid.pcOW = @(sw) pc_corey_lin_buffer_heterogeneous(sw,opt);
   else
       fluid.pcOW = @(sw) pc_corey_nonlin_buffer_heterogeneous(sw,opt);
   end
   
   names = {'W', 'O'};
   for i = 1:numel(names)
       n = names{i};
       bf = @(p, varargin) constantUnitBfactor(p, varargin{:});

       fluid.(['rho', n]) = opt.rho(i);
       fluid.(['rho', n, 'S']) = opt.rho(i);
       fluid.(['b', n]) = bf;
       fluid.(['B', n]) = bf;
       fluid.(['mu', n]) = @(p, varargin) constantViscosity(opt.mu(i), p, varargin{:});
       fluid.(['kr', n]) = relperms{i};
   end
end

% adjust for incmopTPFA
function varargout = properties(opt, varargin)
   varargout{1}                 = opt.mu ;
   if nargout > 1, varargout{2} = opt.rho; end
   if nargout > 2, varargout{3} = opt.n;   end
   if nargout > 3, varargout{4} = opt.sr;  end
   if nargout > 4, varargout{5} = opt.krmax;  end
   if nargout > 5, varargout{6} = opt.regiontags;  end
   if nargout > 6, varargout{7} = opt.np;  end
   if nargout > 7, varargout{8} = opt.pc_scale;  end
   if nargout > 8, varargout{9} = [];  end
end

% function varargout = relperm(s, opt, varargin)
%    [s1, s2, ~] = modified_saturations(s, opt);
% 
%    n   = opt.n;
%    kwm = opt.kwm;
%    varargout{1}    = [ kwm(1) * s1 .^ n(1), kwm(2) * s2 .^ n(2)];
% 
% end
%---------------------------

function [pc] = pc_corey_heterogeneous(sw,opt)

    sr1 = opt.sr{1}(1);
    sr2 = opt.sr{1}(2);
    pc_scale = opt.pc_scale{1}(1);
    np = opt.np{1}(1);       
    pc = pc_corey(sw,pc_scale,np,sr1,sr2);

    if length(opt.n)>1
        for i = 2:length(opt.n)
            sr1 = opt.sr{i}(1);
            sr2 = opt.sr{i}(2);
            pc_scale = opt.pc_scale{i}(1);
            np = opt.np{i}(1);       
            pc(opt.regiontags==i) = pc_corey(sw(opt.regiontags==i),pc_scale,np,sr1,sr2);
        end   
    end
end

function [pc] = pc_corey_lin_buffer_heterogeneous(sw,opt)
   
    sr1 = opt.sr{1}(1);
    sr2 = opt.sr{1}(2);
    pc_scale = opt.pc_scale{1}(1);
    np = opt.np{1}(1);  
    sbuf = opt.sbuf{1}(1);  
    pc = pc_corey_lin_buffer(sw,pc_scale,np,sbuf,sr1,sr2);

    if length(opt.n)>1
        for i = 2:length(opt.n)
            sr1 = opt.sr{i}(1);
            sr2 = opt.sr{i}(2);
            pc_scale = opt.pc_scale{i}(1);
            np = opt.np{i}(1);  
            sbuf = opt.sbuf{i}(1);  
            pc(opt.regiontags==i) = pc_corey_lin_buffer(sw(opt.regiontags==i),pc_scale,np,sbuf,sr1,sr2);
        end      
    end
end

function [pc] = pc_corey_nonlin_buffer_heterogeneous(sw,opt)
    
    sr1 = opt.sr{1}(1);
    sr2 = opt.sr{1}(2);
    pc_scale = opt.pc_scale{1}(1);
    np = opt.np{1}(1);  
    sbuf = opt.sbuf{1}(1);  
    pc = pc_corey_nonlin_buffer(sw,pc_scale,np,sbuf,sr1,sr2);
   
    if length(opt.n)>1
        for i = 2:length(opt.n)
            sr1 = opt.sr{i}(1);
            sr2 = opt.sr{i}(2);
            pc_scale = opt.pc_scale{i}(1);
            np = opt.np{i}(1);  
            sbuf = opt.sbuf{i}(1);  
            pc(opt.regiontags==i) = pc_corey_nonlin_buffer(sw(opt.regiontags==i),pc_scale,np,sbuf,sr1,sr2);
        end         
    end
end

function [krw] = krw_corey(sw,opt,varargin)

    sr1 = opt.sr{1}(1);
    sr2 = opt.sr{1}(2);
    n1 = opt.n{1}(1);
    kwm1 = opt.krmax{1}(1);
    [swe, ~, ~] = modified_saturations(sw, sr1, sr2);

    krw = kwm1*swe.^n1;
    
    if length(opt.n)>1
        for i = 2:length(opt.n)
            sr1 = opt.sr{i}(1);
            sr2 = opt.sr{i}(2);
            n1 = opt.n{i}(1);
            kwm1 = opt.krmax{i}(1);
            [swe, ~, ~] = modified_saturations(sw(opt.regiontags==i), sr1, sr2);

            krw(opt.regiontags==i) = kwm1*swe.^n1;
        end             
    end
end

function [krn] = krn_corey(so,opt,varargin)

    sr1 = opt.sr{1}(1);
    sr2 = opt.sr{1}(2);
    n2 = opt.n{1}(2);
    kwm2 = opt.krmax{1}(2);
    [~, sne, ~] = modified_saturations(1-so, sr1, sr2);

    krn = kwm2*sne.^n2;
    if length(opt.n)>1
        for i = 2:length(opt.n)
            sr1 = opt.sr{i}(1);
            sr2 = opt.sr{i}(2);
            n2 = opt.n{i}(2);
            kwm2 = opt.krmax{i}(2);
            [~, sne, ~] = modified_saturations(1-so(opt.regiontags==i), sr1, sr2);

            krn(opt.regiontags==i) = kwm2*sne.^n2;

        end
    end

end

function [krW, krO] = relPerm(krW, krO, sw, varargin)
    krW = krW(sw, varargin{:});
    krO = krO(1 - sw, varargin{:});
end

function B = constantUnitBfactor(p, varargin)
    B = p*0 + 1;
end

function mu = constantViscosity(mu, p, varargin)
    mu = p*0 + mu;
end

function [swe, sne, den] = modified_saturations(sw, srw, srn)
   den = 1 - (srw+srn);
   swe  = (    sw - srw) ./ den;  swe(swe < 0) = 0;  swe(swe > 1) = 1;
   sne  = (1 - sw - srn) ./ den;  sne(sne < 0) = 0;  sne(sne > 1) = 1;
end
