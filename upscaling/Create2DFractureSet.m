function [ f ] = Create2DFractureSet( G, nx, ny, frac_offset, type)
%CREATE2DFRACTURESET Summary of this function goes here
%   Detailed explanation goes here

% type = 'orthogonal';

dims = find(G.cartDims);
physDims = zeros(size(G.cartDims));

for i = dims
    physDims(i) = max(G.nodes.coords(:,i))-min(G.nodes.coords(:,i));
end
dx = physDims(1)/nx;
dy = physDims(2)/ny;
f = zeros(2*nx*ny,4);

if (strcmp(type ,'orthogonal'))
    
    % fractures lines in [x1 y1 x2 y2] format.
    k=1;
    for i=1:nx
        for j=1:ny
            if(mod(j,2)==0)
                f(k,1) = (i-1)*dx + frac_offset*dx;
                f(k,2) = (j-1)*dy + dy/2;
                f(k,3) = (i-1)*dx + dx/2 - frac_offset*dx;
                f(k,4) = (j-1)*dy + dy/2;
                f(k+1,1) = (i-1)*dx + 3/4*dx;
                f(k+1,2) = (j-1)*dy + frac_offset*dy;
                f(k+1,3) = (i-1)*dx + 3/4*dx;
                f(k+1,4) = (j-1)*dy + (1-frac_offset)*dy;
                k = k+2;
            else
                f(k,1) = (i-1)*dx + dx/4;
                f(k,2) = (j-1)*dy + frac_offset*dy;
                f(k,3) = (i-1)*dx + dx/4;
                f(k,4) = (j-1)*dy + (1-frac_offset)*dy;
                f(k+1,1) = (i-1)*dx + dx/2+frac_offset*dx;
                f(k+1,2) = (j-1)*dy + dy/2;
                f(k+1,3) = (i-1)*dx + (1-frac_offset)*dx;
                f(k+1,4) = (j-1)*dy + dy/2;
                k = k+2;

            end
        end
    end
end
if (strcmp(type ,'intersection_orth'))
    % fractures lines in [x1 y1 x2 y2] format.
    k=1;
    for i=1:nx
        for j=1:ny
            if(mod(j,2)==0)
                f(k,1) = (i-1)*dx + frac_offset*dx;
                f(k,2) = (j-1)*dy + dy/2;
                f(k,3) = (i-1)*dx + dx - frac_offset*dx;
                f(k,4) = (j-1)*dy + dy/2;
                f(k+1,1) = (i-1)*dx + 3/4*dx;
                f(k+1,2) = (j-1)*dy + frac_offset*dy;
                f(k+1,3) = (i-1)*dx + 3/4*dx;
                f(k+1,4) = (j-1)*dy + (1-frac_offset)*dy;
                k = k+2;
            else
                f(k,1) = (i-1)*dx + dx/4;
                f(k,2) = (j-1)*dy + frac_offset*dy;
                f(k,3) = (i-1)*dx + dx/4;
                f(k,4) = (j-1)*dy + (1-frac_offset)*dy;
                f(k+1,1) = (i-1)*dx +frac_offset*dx;
                f(k+1,2) = (j-1)*dy + dy/2;
                f(k+1,3) = (i-1)*dx + (1-frac_offset)*dx;
                f(k+1,4) = (j-1)*dy + dy/2;
                k = k+2;

            end
        end
    end
end
if (strcmp(type ,'network_orth'))
    % fractures lines in [x1 y1 x2 y2] format.
    k=1;
    for i=1:nx
        f(k,1) = (i-1)*dx + dx/4;
        f(k,2) = frac_offset*dy;
        f(k,3) = (i-1)*dx + dx/4;
        f(k,4) = (1-frac_offset)*physDims(2);
        k=k+1;
    end
    for j=1:ny
        f(k,1) = frac_offset*dx;
        f(k,2) = (j-1)*dy + dy/2;
        f(k,3) = (1-frac_offset)*physDims(1);
        f(k,4) = (j-1)*dy + dy/2;
        k = k+1;
    end

end
end

