function fluid = initADIFluidUpsRelperm(varargin)
% SYNOPSIS:
%   fluid = initSimpleADIFluidUpsRelperm()
%   fluid = initSimpleADIFluidUpsRelperm('pn1', 'pv1')
%
% PARAMETERS:
%   'pn'/pv - List of property names/property values.  Possibilities are:
%   - mu  : vector of viscosity values for water, oil and gas, [muW, muO, muG].  
%           Default is [1 1 1].
%   - rho : vector of density values for water, oil and gas, [rhoW, rhoO, rhoG].
%           Default is [1 1 1].
%   - n   : vector of the degrees of the monomials describing relative
%           permeability for water, oil and gas [nW, nO, nG].  Default is
%           [1 1 1] (linear relative permeabilities)
%
% RETURNS:
%   fluid - struct containing the following functions (where X = 'W' [water],
%           'O' [oil] and 'G' [gas]) 
%           * [krW, krO, krG] = relPerm(s_water, s_gas) - relative permeability functions         
%           * rhoX         - density of X
%           * rhoXS        - density of X at surface (equal to rhoX, since incompressible)
%           * bX(p), BX(p) - formation volume factors and their inverses
%                            (constants, always equal one)
%           * muX(p)       - viscosity functions (constant)
%           * krX(s)       - rel.perm for X
%           * krOX(s)      - 
%           * rsSat()      - saturation value for dissolved gas (always returns 0)

   opt = struct('mu', [1 1], 'rho', [1 1], 'sr',[0 0],...
                'sw', [], 'krw', [], 'krn',[],... 
                'np', 1,'pc_scale', 0,...
                'c',       [], ...
                'pRef',    0, ...
                'cR',      []);
            
   opt = merge_options(opt, varargin{:});
   
   krW = @(sw, varargin) krw_ups(sw,opt,varargin{:});
   krO = @(so, varargin) krn_ups(so,opt,varargin{:});
   
   relperms = {krW, krO};

   fluid.relPerm = @(sw, sg, varargin) relPerm(krW, krO, sw, varargin{:});

   % adjust for incompTPFA
   prop = @(  varargin) properties(opt, varargin{:});
   %---------------------------
   % adjust for incompTPFA
   fluid.properties = prop;
   fluid.saturation = @(x,varargin) x.s;
%    fluid.relperm = kr;
   fluid.relperm = @(sw, sg, varargin) relPerm(krW, krO, sw, varargin{:});
   %---------------------------
   
   fluid.pcOW = @(sw) pc_corey2(sw,opt);
   
   names = {'W', 'O'};
   for i = 1:numel(names)
       n = names{i};
       bf = @(p, varargin) constantUnitBfactor(p, varargin{:});

       fluid.(['rho', n]) = opt.rho(i);
       fluid.(['rho', n, 'S']) = opt.rho(i);
       fluid.(['b', n]) = bf;
       fluid.(['B', n]) = bf;
       fluid.(['mu', n]) = @(p, varargin) constantViscosity(opt.mu(i), p, varargin{:});
       fluid.(['kr', n]) = relperms{i};
   end
end

% adjust for incmopTPFA
function varargout = properties(opt, varargin)
   varargout{1}                 = opt.mu ;
   if nargout > 1, varargout{2} = opt.rho; end
   if nargout > 2, varargout{3} = []     ; end
end
% function varargout = relperm(s, opt, varargin)
%    [s1, s2, ~] = modified_saturations(s, opt);
% 
%    n   = opt.n;
%    kwm = opt.kwm;
%    varargout{1}    = [ kwm(1) * s1 .^ n(1), kwm(2) * s2 .^ n(2)];
% 
% end
%---------------------------

function [pc] = pc_corey2(sw,opt)
   
   pc = 0;
   
end


function [krw] = krw_ups(sw,opt,varargin)

    [swe, ~, ~] = modified_saturations(sw, opt.sr(1), opt.sr(2));
    
    table = [opt.sw opt.krw opt.krn];

    [krw, ~, ~, ~, ~, ~, ~, ~, ~] = swof(table);

    krw = krw{1};
    krw = krw(swe);    
end

function [krn] = krn_ups(so,opt,varargin)

    [~, sne, ~] = modified_saturations(1-so, opt.sr(1), opt.sr(2));
    table = [opt.sw opt.krw opt.krn];

    [~, krn, ~, ~, ~, ~, ~, ~, ~] = swof(table);

    krn = krn{1};
    krn = krn(sne);    
end

function [krW, krO] = relPerm(krW, krO, sw, varargin)
    krW = krW(sw, varargin{:});
    krO = krO(1 - sw, varargin{:});
end

function B = constantUnitBfactor(p, varargin)
    B = p*0 + 1;
end

function mu = constantViscosity(mu, p, varargin)
    mu = p*0 + mu;
end

function [swe, sne, den] = modified_saturations(sw, srw, srn)
   den = 1 - (srw+srn);
   swe  = (    sw - srw) ./ den;  swe(swe < 0) = 0;  swe(swe > 1) = 1;
   sne  = (1 - sw - srn) ./ den;  sne(sne < 0) = 0;  sne(sne > 1) = 1;
end
