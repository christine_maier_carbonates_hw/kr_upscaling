clear all; 
close all;
% Load necessary modules, etc 
mrstModule add hfm;             % hybrid fracture module
mrstModule add mrst-gui;        % plotting routines
mrstModule add ad-props ad-core % AD framework
mrstModule add ad-blackoil      % Three phase simulator
checkLineSegmentIntersect;      % ensure lineSegmentIntersect.m is on path
mrstModule add incomp

%% Grid and fracture lines
% Construct a Cartesian grid comprising 50-by-20 cells, where each cell has
% dimension 10-by-10 m^2. Define 3 fracture lines by supplying their end
% points in the form [x1 y1 x2 y2].

celldim = [6 3];
physdim = [20 20];
G = cartGrid(celldim, physdim);
G = computeGeometry(G);


fl = Create2DFractureSet(G,2,2, 0.1, 'intersection_orth');


%% Process fracture lines
% Using the input fracture lines, we identify independent fracture networks
% comprising of connected lines. In this example, there is only 1 fracture
% network consisting of 3 fracture lines. We also identify the fine-cells
% in the matrix containing these fractures. Fracture aperture is set to
% 0.04 meters. The matrix grid and fracture lines are plotted.

dispif(mrstVerbose, 'Processing user input...\n\n');
[G,fracture] = processFracture2D(G, fl, 'verbose', mrstVerbose);
fracture.aperture = 0.001; % NOTE: Artificially large aperture for plotting purposes
figure;
plotFractureLines(G,fracture);
axis equal tight; 
box on

%% Compute CI and construct fracture grid
% For each matrix block containing a fracture, we compute a fracture-matrix
% conductivity index (CI) in accordance with the hierarchical fracture
% model (a.k.a. embedded discrete fracture model). Following that, we
% compute a fracture grid where the fracture cell size is defined to be
% 10 m. Next, the fracture grid is plotted on top of the matrix grid.

dispif(mrstVerbose, 'Computing CI and constructing fracture grid...\n\n');
G = CIcalculator2D(G,fracture);
min_size = 0.01; cell_size = .1; % minimum and average cell size.
[G,F,fracture] = gridFracture2D(G,fracture,'min_size',min_size,'cell_size',cell_size);
clf; plotFractureNodes2D(G,F,fracture); 
axis equal tight; box on


%% Set rock properties in fracture and matrix
%% Rock
K = .1*milli*darcy;
porosity = 0.1;
K_frac = 100; % Darcy
porosity_frac = 0.99;
rock = makeRock(G,K,porosity);

% rock.perm = rand(G.cells.num,1)*10*milli*darcy;
p = porosity*ones(G.cells.num,1);
K = K*ones(G.cells.num,1);

G.rock.poro = p(:);
G.rock.perm = K(:);


% G = makeRockFrac(G, K_frac,'permtype', 'heterogeneous', 'porosity', 0.99);
G = makeRockFrac(G, K_frac, 'porosity', porosity_frac);

figure()
plotCellData(G,G.rock.perm(:))

[G,T] = defineNNCandTrans(G,F,fracture);

%% Compute upscaled permeability
Keff = upscale_perm(G,G.rock,T);

%% Fluid
muw = 1*centi*poise;
mun = 2*centi*poise;

%Here you define the region id for each cell                       
regiontags = zeros(G.cells.num,1);    
regiontags(1:G.Matrix.cells.num) = 1;
for i = 1:size(F,2)
%     regiontags(F(i).cells.start:F(i).cells.start+F(i).cells.num-1) = i+1;
    regiontags(F(i).cells.start:F(i).cells.start+F(i).cells.num-1) = 2;
end
figure();
plotCellData(G,regiontags);
xlabel('x')
ylabel('y')
zlabel('z')

%% fluid
% Here you define corey parameters, one element for each region
n = {[2,2],...
     [1,1]...
%     ,[1.4,1.4],...
%     [1.6,1.6]...
     };
sr = {[0.0,0.0],...
      [0.0,0.0]...
%      ,[0,0],...
%      [0,0]
      };
kwm = {[0.3,0.8],...
       [1,1]...
%       ,[1,1],...
%       [1,1]
       };
np = {-1,-1};
pc_scale = {0,0};
fluid = initHeterogeneousCoreyFluid('mu' , [  muw, mun],...
                           'rho', [1000,1000]*kilogram/meter^3,...
                           'n'  , n, ...
                           'sr' , sr, ...
                           'kwm', kwm, ... 
                           'regiontags', regiontags);     
upscaled_ADIfluid = initADIFluidHeterogeneous('mu' , [  muw, mun],...
                           'rho', [1000,1000]*kilogram/meter^3,...
                           'n'  , n, ...
                           'sr' , sr, ...
                           'krmax', kwm, ... 
                           'np', np, ... 
                           'pc_scale', pc_scale, ... 
                           'regiontags', regiontags); 
%% upscale perm for different fractional flow levels
Fw = [0:0.1:1];

sw = zeros(size(Fw'));
krw = zeros([size(Fw,2) size(G.cartDims,2)]);
krn = zeros([size(Fw,2) size(G.cartDims,2)]);
for i=1:size(Fw,2)
    [sw(i), krw(i,:), krn(i,:)] = upscale_relperm(G, G.rock, T, fluid, Keff, Fw(i));
end


%% plot relperm 

dims = find(G.cartDims);
dir = ['x','y','z'];

figure1 = figure();
axes1 = axes('Parent',figure1);
hold on;
for i=1:max(regiontags(:))
    plot(sw,kwm{i}(1)*sw.^n{i}(1),'DisplayName',['krw zone ',num2str(i)],'LineStyle','-.','LineWidth',2);
    plot(sw,kwm{i}(2)*(1-sw).^n{i}(2),'DisplayName',['krn zone ',num2str(i)],'LineStyle','-.','LineWidth',2);
end

for i=dims
    plot(sw, krw(:,i),'DisplayName',['upscaled krw_',dir(i)],'LineStyle','-','LineWidth',2);
    plot(sw, krn(:,i),'DisplayName',['upscaled krn_',dir(i)],'LineStyle','-','LineWidth',2);
end
xlabel('Saturation Water [-]','FontWeight','bold');
ylabel('Relative Permeability [-]','FontWeight','bold');
legend1 = legend('show');
set(legend1,'Location','best');
set(axes1,'FontSize',20,'FontWeight','bold','XGrid','on','YGrid','on');

% save('int_frac_16_16_100InterpPoints.mat','G','rock','fluid', 'upscaled_ADIfluid','Keff', 'sw', 'krw', 'krn');

[err_ups, max_err_ups,simtime_ratio] = UpscaleError( G, G.rock, T, upscaled_ADIfluid,...
    Keff, sw, krw, krn);
