load('RossenKumarRelPerm.mat')



%% get brooks corey coeffs from the upscaled relperms

krw_fun = @(coeffs,s)coeffs(1)*s.^coeffs(2);
krn_fun = @(coeffs,s)coeffs(1)*(1-s).^coeffs(2);

sw0 = [0,1];
krwmax_fit = zeros(1,6);
krnmax_fit = zeros(1,6);
nw_fit = zeros(1,6);
nn_fit = zeros(1,6);
for i = 1:6
    x = lsqcurvefit(krw_fun,sw0,HD(i).sw,HD(i).krw);
    y = lsqcurvefit(krn_fun,sw0,HD(i).sw,HD(i).krn);

    krwmax_fit(i) = x(1);
    krnmax_fit(i) = y(1);
    nw_fit(i) = x(2);
    nn_fit(i) = y(2);
end


for i=1:6
    HD(i).kwm = [min(krwmax_fit(i),1.0),min(krnmax_fit(i),1.0)];
    HD(i).n = [nw_fit(i),nn_fit(i)];
end
%% check interpolated rel perms

% krw_interp = @(sw,i)krwmax_fit(i)*sw.^nw_fit(i);
% krn_interp = @(sw,i)krnmax_fit(i)*(1-sw).^nn_fit(i);
krw_interp = @(sw,i)HD(i).kwm(1)*sw.^HD(i).n(1);
krn_interp = @(sw,i)HD(i).kwm(2)*(1-sw).^HD(i).n(2);
sw = [0:0.01:1];

figure();
hold; 
for i=1:6
plot(HD(i).sw, HD(i).krw,'DisplayName',['krw_', int2str(i)],'LineStyle','-','LineWidth',1);
plot(HD(i).sw, HD(i).krn,'DisplayName',['krn_' int2str(i)],'LineStyle','-','LineWidth',1);
plot(sw, krw_interp(sw,i),'linestyle','none','marker','o','linewidth',2.0)
plot(sw, krn_interp(sw,i),'linestyle','none','marker','o','linewidth',2.0)
end
xlabel('Saturation Water [-]');
ylabel('Relative Permeability [-]');
legend1 = legend('show');
set(legend1,'Location','best');


save('RossenKumarRelPerm.mat','HD')