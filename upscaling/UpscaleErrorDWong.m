function [ err_ups, max_err_ups, simtime_ratio ] = UpscaleErrorDWong( G, rock, Trans, fluid, Keff, sw, krw, krn, a_tol_min, a_tol_max)
%UPSCALEERROR Summary of this function goes here
%   Detailed explanation goes here
mrstModule add mrst-gui;        % plotting routines

plot_sats = false;

dims = find(G.cartDims);
physDims = zeros(size(G.cartDims));

for i = dims
    physDims(i) = max(G.nodes.coords(:,i))-min(G.nodes.coords(:,i));
end

G_ups = cartGrid(ceil(G.cartDims),physDims);
G_ups = computeGeometry(G_ups);

[mu, roh] = fluid.properties();
   

bside = {{'XMin', 'XMax'}, {'YMin', 'YMax'}, {'ZMin', 'ZMax'}};    
frac_bside = {{'West', 'East'}, {'South', 'North'}, {'Top', 'Bottom'}};    

bfaces = cell(2,G.griddim);  
frac_bfaces=findfracboundaryfaces(G,1.0e-5);   
bfaces_ups = cell(2,G.griddim);


%% Solver
solver = NonLinearSolver();
%% initial condition 
pinit = zeros(G.cells.num,1);
sinit = [zeros(G.cells.num,1),ones(G.cells.num,1)];
finit = zeros(G.faces.num, 1);
pinit_ups = zeros(G_ups.cells.num,1);
sinit_ups = [zeros(G_ups.cells.num,1),ones(G_ups.cells.num,1)];
finit_ups = zeros(G_ups.faces.num, 1);


%% fixed flux boundary conditions
tpv = sum(G.cells.volumes.*rock.poro);
p2 = 0*Pascal;

%time step
dt = 1*day;
tmax = 100*dt; 
rec_max = tpv;

total_flux = 5*tpv/tmax;
% no wells
W= [];



%%
dt_fg = rampupTimesteps(tmax, 1*day, 10);
%%

% rec = zeros(G.griddim,tmax/dt+1);
% water_rate = zeros(G.griddim,tmax/dt+1);
% oil_rate = zeros(G.griddim,tmax/dt+1);
% water_rec = zeros(G.griddim,tmax/dt+1);
% oil_rec = zeros(G.griddim,tmax/dt+1);
% 
% rec_ups = zeros(G.griddim,tmax/dt+1);
% water_rate_ups = zeros(G.griddim,tmax/dt+1);
% oil_rate_ups = zeros(G.griddim,tmax/dt+1);
% water_rec_ups = zeros(G.griddim,tmax/dt+1);
% oil_rec_ups = zeros(G.griddim,tmax/dt+1);

rec = zeros(G.griddim,size(dt_fg,1)+1);
water_rate = zeros(G.griddim,size(dt_fg,1)+1);
oil_rate = zeros(G.griddim,size(dt_fg,1)+1);
water_rec = zeros(G.griddim,size(dt_fg,1)+1);
oil_rec = zeros(G.griddim,size(dt_fg,1)+1);

rec_ups = zeros(G.griddim,size(dt_fg,1)+1);
water_rate_ups = zeros(G.griddim,size(dt_fg,1)+1);
oil_rate_ups = zeros(G.griddim,size(dt_fg,1)+1);
water_rec_ups = zeros(G.griddim,size(dt_fg,1)+1);
oil_rec_ups = zeros(G.griddim,size(dt_fg,1)+1);

% time_vctr = zeros(1,tmax/dt+1);
time_vctr = zeros(1,size(dt_fg,1)+1);
time_vctr_ups = zeros(1,size(dt_fg,1)+1);
err_ups = zeros(G.griddim,size(dt_fg,1)+1);

telapsed = zeros(1,G.griddim);
telapsed_ups = zeros(1,G.griddim);

cross_section_x = find(G.cells.centroids(:,1)>=physDims(1)/2 &...
                     G.cells.centroids(:,1)<=physDims(1)/2+physDims(1)/G_ups.cartDims(1));
cross_section_y = find(G.cells.centroids(:,2)>=physDims(2)/2 &...
                     G.cells.centroids(:,2)<=physDims(2)/2+physDims(2)/G_ups.cartDims(2));
cross_section_x_ups = find(G_ups.cells.centroids(:,1)>=physDims(1)/2 &...
                     G_ups.cells.centroids(:,1)<=physDims(1)/2+physDims(1)/G_ups.cartDims(1));
cross_section_y_ups = find(G_ups.cells.centroids(:,2)>=physDims(2)/2 &...
                     G_ups.cells.centroids(:,2)<=physDims(2)/2+physDims(2)/G_ups.cartDims(2));
Sx = zeros(size(dt_fg,1)+1,size(cross_section_x,1));
Sy = zeros(size(dt_fg,1)+1,size(cross_section_y,1));
Sx_ups = zeros(size(dt_fg,1)+1,size(cross_section_x_ups,1));
Sy_ups = zeros(size(dt_fg,1)+1,size(cross_section_y_ups,1));

if(plot_sats)
    h1 = figure('Position',[100,100,1200,600]);
    h2 = figure('Position',[100,100,1200,600]);
    [hms,hfs]=deal([]);
end



for i = 1:G.griddim
    model = TwoPhaseOilWaterModel(G, [], fluid);
    if ~isempty(Trans)
        N = getNeighbourship(G, 'topological', true);
        intx = all(N ~= 0, 2);

        % Send in internal transmissibility and neighborship to the operator setp
        model.operators = setupOperatorsTPFA(G, G.rock, 'trans', Trans(intx), 'neighbors', N(intx, :));
        model.operators.T_all = Trans;
    end
    state = struct('pressure', pinit, 's', sinit, 'flux', finit);
    state.wellSol = initWellSolAD(W,model,state);
    
    bc_tmp = fluxside([],G,bside{i}{1}, total_flux, 'sat', [1,0]);
    bc = fluxside_hfm(G, bc_tmp.face, frac_bfaces.(frac_bside{i}{1}), total_flux, 'sat', [1,0]);
    bfaces{1,i} = bc.face;
    
    bc_tmp = pside([],G,bside{i}{2}, p2, 'sat', [0,1]);
    bfaces{2,i} = [bc_tmp.face; frac_bfaces.(frac_bside{i}{2})'];
    bc = addBC(bc,bc_tmp.face,bc_tmp.type,bc_tmp.value,'sat',bc_tmp.sat);
    bc = addBC(bc,frac_bfaces.(frac_bside{i}{2}),'pressure',p2, 'sat', [0,1]);

    %upscaled model 
    rock_ups  = makeRock(G_ups,Keff(i),tpv/sum(G_ups.cells.volumes));

    fluid_ups = initADIFluidUpsRelperm('mu' , [  mu(1), mu(2)],...
                           'rho', [roh(1),roh(2)],...
                           'sw', sw,...
                           'krw', krw(:,i),...
                           'krn',krn(:,i));
                       
    model_ups = TwoPhaseOilWaterModel(G_ups, rock_ups, fluid_ups);
    state_ups = struct('pressure', pinit_ups, 's', sinit_ups, 'flux', finit_ups);
    state_ups.wellSol = initWellSolAD(W,model_ups,state_ups);
    
    bc_tmp = fluxside([],G_ups,bside{i}{1}, total_flux, 'sat', [1,0]);
    bfaces_ups{1,i} = bc_tmp.face;
    bc_ups = addBC([],bc_tmp.face,bc_tmp.type,bc_tmp.value,'sat',bc_tmp.sat);
    
    bc_tmp = pside([],G_ups,bside{i}{2}, p2, 'sat', [0,1]);
    bfaces_ups{2,i} = bc_tmp.face;
    bc_ups = addBC(bc_ups,bc_tmp.face,bc_tmp.type,bc_tmp.value,'sat',bc_tmp.sat);
    

    %% Time integration
    tstart = tic;
    schedule = simpleSchedule(dt_fg,'bc',bc);
    [ws, states, report] = simulateScheduleAD(state, model, schedule);

    time_vctr(1) = report.ReservoirTime(1);
    
    for j=2:size(states,1)
        rec(i,j) = sum(G.cells.volumes.*rock.poro.*states{j}.s(:,1))/rec_max;
        water_rate(i,j) = sum(states{j}.flux(bfaces{2,i},1));
        oil_rate(i,j)  = sum(states{j}.flux(bfaces{2,i},2));
        water_rec(i,j) = water_rec(i,j-1) + water_rate(i,j)*(report.ReservoirTime(j)-report.ReservoirTime(j-1));
        oil_rec(i,j)  = oil_rec(i,j-1) + oil_rate(i,j)*(report.ReservoirTime(j)-report.ReservoirTime(j-1));

        if(i==1)
            Sy(j,:) = states{j}.s(cross_section_y,1);
        end
        if(i==2)
            Sx(j,:) = states{j}.s(cross_section_x,1);
        end
        time_vctr(j) = report.ReservoirTime(j);
    end
%     while t < tmax
%         state = solver.solveTimestep(state, dt, model, 'bc',bc);
%         
%         rec(i,j) = sum(G.cells.volumes.*rock.poro.*state.s(:,1))/rec_max;
%         water_rate(i,j) = sum(state.flux(bfaces{2,i},1));
%         oil_rate(i,j)  = sum(state.flux(bfaces{2,i},2));
%         water_rec(i,j) = water_rec(i,j-1) + water_rate(i,j)*dt;
%         oil_rec(i,j)  = oil_rec(i,j-1) + oil_rate(i,j)*dt;
% 
%         if(i==1)
%             Sy(j,:) = state.s(cross_section_y,1);
%         end
%         if(i==2)
%             Sx(j,:) = state.s(cross_section_x,1);
%         end
% 
% 
%         if(plot_sats)
%             figure(h1); 
%             subplot(1,3,1);
%             delete(hfs);
%             hfs = plotCellData(G, state.s(:, 1),G.Matrix.cells.num+1:G.cells.num);
%             view(-28,40);
%             title(['Water saturation'])
%             colorbar;
%             caxis([0,1])
%             drawnow;
%             subplot(1,3,2);
%             delete(hms);
%             hms = plotCellData(G, state.s(:, 1),state.s(:, 1)>1e-3);
%             xlim([0 physDims(1)]);
%             ylim([0 physDims(2)]);
%             view(-28,40);
%             title(['Water saturation'])
%             colorbar;
%             caxis([0,1])
%             drawnow;
%             subplot(1,3,3);
%             plotCellData(G, state.pressure(:));
%             view(-28,40);
%             title(['Pressure'])
%             colorbar;
%             drawnow;
%         end
%         time_vctr(j) = t+dt;
%         t = t+dt;
%         j = j+1;
% 
%     end
    telapsed(i) = toc(tstart);

%     t = 0;
%     j=2;
    tstart = tic;
    schedule_ups = simpleSchedule(dt_fg,'bc',bc_ups);
    [ws_ups, states_ups, report_ups] = simulateScheduleAD(state_ups, model_ups, schedule_ups);

    time_vctr_ups(1) = report_ups.ReservoirTime(1);
    
    for j=2:size(states_ups,1)
        rec_ups(i,j) = sum(G_ups.cells.volumes.*rock_ups.poro.*states_ups{j}.s(:,1))/rec_max;
        water_rate_ups(i,j) = sum(states_ups{j}.flux(bfaces_ups{2,i},1));
        oil_rate_ups(i,j)  = sum(states_ups{j}.flux(bfaces_ups{2,i},2));
        water_rec_ups(i,j) = water_rec_ups(i,j-1) + water_rate_ups(i,j)*(report_ups.ReservoirTime(j)-report_ups.ReservoirTime(j-1));
        oil_rec_ups(i,j)  = oil_rec_ups(i,j-1) + oil_rate_ups(i,j)*(report_ups.ReservoirTime(j)-report_ups.ReservoirTime(j-1));

        if(i==1)
            Sy_ups(j,:) = states_ups{j}.s(cross_section_y_ups,1);
        end
        if(i==2)
            Sx_ups(j,:) = states_ups{j}.s(cross_section_x_ups,1);
        end
        time_vctr_ups(j) = report_ups.ReservoirTime(j);
    end

%     while t < tmax
%         state_ups = solver.solveTimestep(state_ups, dt, model_ups, 'bc',bc_ups);
%         
%         rec_ups(i,j) = sum(G_ups.cells.volumes.*rock_ups.poro.*state_ups.s(:,1))/rec_max;
%         water_rate_ups(i,j) = sum(state_ups.flux(bfaces_ups{2,i},1));
%         oil_rate_ups(i,j)  = sum(state_ups.flux(bfaces_ups{2,i},2));
%         water_rec_ups(i,j) = water_rec_ups(i,j-1) + water_rate_ups(i,j)*dt;
%         oil_rec_ups(i,j)  = oil_rec_ups(i,j-1) + oil_rate_ups(i,j)*dt;
%         
%         if(i==1)
%             Sy_ups(j,:) = state_ups.s(cross_section_y_ups,1);
%         end
%         if(i==2)
%             Sx_ups(j,:) = state_ups.s(cross_section_x_ups,1);
%         end
% 
%         if(plot_sats)
%             figure(h2); 
%             subplot(1,2,1);        
%             delete(hfs);
%             hfs = plotCellData(G_ups, state_ups.s(:, 1));
%             view(-28,40);
%             title(['Water saturation'])
%             colorbar;
%             caxis([0,1])
%             drawnow;
%             subplot(1,2,2);
%             plotCellData(G_ups, state_ups.pressure(:));
%             view(-28,40);
%             title(['pressure'])
%             colorbar;
%             drawnow;
%         end
% 
%         time_vctr(j) = t+dt;
%         t = t+dt;
%         j = j+1;
%     end
    telapsed_ups(i) = toc(tstart);

    err_ups(i,:) = abs(rec_ups(i,:)-rec(i,:))./rec(i,:)*100;      
    
end

max_err_ups = zeros(1,G.griddim);
for i = 1:G.griddim
    max_err_ups(i) = max(err_ups(i,:))
end
simtime_ratio = telapsed_ups./telapsed

%% plotting figures
dir = ['x','y','z'];
figure();
pv_inj = total_flux.*time_vctr./tpv;
for i = 1:G.griddim
    s =subplot(1,G.griddim,i);
    hold on;
    plot(pv_inj, water_rate(i,:)./stb.*day, 'DisplayName', ['water rate dir_',dir(i)],'LineWidth',2)
    plot(pv_inj, water_rate_ups(i,:)./stb.*day, 'DisplayName', ['upscaled water rate dir_',dir(i)],'LineWidth',2)
    plot(pv_inj, oil_rate(i,:)./stb.*day,'DisplayName', ['oil rate dir_',dir(i)],'LineWidth',2)
    plot(pv_inj, oil_rate_ups(i,:)./stb.*day,'DisplayName', ['upscaled oil rate dir_',dir(i)],'LineWidth',2)
    xlabel('porevolume injected [-]','FontWeight','bold');
    ylabel('production rates [stb/day]','FontWeight','bold');
    legend1 = legend('show');
    set(legend1,'Location','best');
    set(s,'FontSize',20,'FontWeight','bold');
end

figure(); 
for i = 1:G.griddim
    s = subplot(1,G.griddim,i);
    hold on;
    plot(pv_inj, water_rec(i,:)./stb, 'DisplayName', ['water recovery dir_',dir(i)],'LineWidth',2)
    plot(pv_inj, oil_rec(i,:)./stb, 'DisplayName', ['oil recovery dir_',dir(i)],'LineWidth',2)
    plot(pv_inj, water_rec_ups(i,:)./stb, 'DisplayName', ['upscaled water recovery dir_',dir(i)],'LineWidth',2)
    plot(pv_inj, oil_rec_ups(i,:)./stb,'DisplayName', ['upscaled oil recovery dir_',dir(i)],'LineWidth',2)
    xlabel('porevolume injected [-]','FontWeight','bold');
    ylabel('oil and water recoveries [stb]','FontWeight','bold');
    legend1 = legend('show');
    set(legend1,'Location','best');
    set(s,'FontSize',20,'FontWeight','bold');
end

if(plot_sats)
    figure();
    subplot(1,2,1);
    hold on;
    plot(G.cells.centroids(cross_section_y,1), Sy(end,:),'DisplayName','fine grid','MarkerSize',10,'Marker','o',...
        'LineStyle','none');
    plot(G_ups.cells.centroids(cross_section_y_ups,1), Sy_ups(end,:),'DisplayName','coarse grid','Marker','o');
    title(['Water saturation in x'])
    colorbar;
    ylim([0,1])
    drawnow;
    subplot(1,2,2);
    hold on;
    plot(G.cells.centroids(cross_section_x,2), Sx(end,:),'DisplayName','fine grid','MarkerSize',10,'Marker','o',...
        'LineStyle','none');
    plot(G_ups.cells.centroids(cross_section_x_ups,2), Sx_ups(end,:),'DisplayName','coarse grid','Marker','o');
    title(['Water saturation in y'])
    colorbar;
    ylim([0,1])
    drawnow;
end

%% plotting error 
figure1 = figure();
axes1 = axes('Parent',figure1);
hold on;
pv_inj = total_flux.*time_vctr./tpv;
for i = 1:G.griddim
    plot(pv_inj,err_ups(i,:),'MarkerFaceColor',[0 0 1],'MarkerSize',3,'Marker','o',...
        'LineStyle','-','LineWidth', 2, 'DisplayName', ['direction ',dir(i)]);
end
xlabel('porevolume injected [/]','FontWeight','bold');
ylabel('relative error for oil recovery factor [%]','FontWeight','bold');
legend('show');
set(axes1,'FontSize',20,'FontWeight','bold','XGrid','on','YGrid','on');

save(['DWong_prod_data_a_' int2str(a_tol_min) '_' int2str(a_tol_max) '.mat'],'pv_inj', 'water_rate','oil_rate', 'water_rate_ups','oil_rate_ups', 'water_rec','oil_rec', 'water_rec_ups','oil_rec_ups','telapsed_ups','telapsed');
end

function  bc = fluxside_hfm(G, side_faces, bc_frac_faces, flux, varargin)


opt = struct('sat', [], 'range', []);
opt = merge_options(opt, varargin{:});
sat = opt.sat;

bfaces = [side_faces; bc_frac_faces'];

if size(sat,1) == 1, sat = sat(ones([numel(bfaces), 1]), :); end

a  = G.faces.areas(bfaces);
sa = sum(a);
bc = addBC([], bfaces, 'flux', (flux / sa) .* a, 'sat', sat);

end
